.. raw:: html

   <!-- (php # 2 __DATE__ Vekstens paradoks) -->
   <!-- ($attrib_AC=0;) -->

=================
Vekstens paradoks
=================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

.. Avsnitt G20-mangler-klar-visjon

Økonomer, politikere og samfunnseksperter er stort sett enige om at
vi må løse framtidens samfunnsutfordringer gjennom økonomisk vekst.
Verdensøkonomien har kommet i en tilstand av vedvarende krise etter
finanskrisen i 2008.
G20-toppmøtet i Kina i september 2016 stadfestet nok en gang at verdens
ledende økonomer ikke klarer å framskaffe det riktige verktøyet for
å få økonomien på rett kjøl igjen.
Som alltid er det den svake veksten en prøver å fikse.
Håpet er at neste generasjon av teknologiske framskritt
skal sørge for ny etterspørsel.

.. Avsnitt IMF-OECD-om-ulikheter

IMF og OECD har satt nødvendigheten av fortsatt vekst høyt oppe på agendaen.
Begge kom med advarselen om at økte ulikheter hemmer veksten, våren 2015.
Økt fokus på ulikheter har kommet som følge av Thomas Piketty sin bok
*Kapitalen i det 21. århundre* og andre forfattere som tar opp dette temaet.

.. Avsnitt Ulikheter-Piketty-og-Polanyi

Det finnes mange viktige argumenter for å jevne ut ulikheter.
Både Thomas Piketty og Karl Polanyi (i *Den liberale utopi*)
påpeker hvordan ulikheter på slutten av 1800-tallet
økte og at det samtidig var økende gnisninger i verdens-økonomien.
Polanyi forklarer at i løpet av 1800-tallet var internasjonal
handel fredsbevarende fordi "storkapitalen", altså
internasjonale eiere, ikke så seg tjent med krigsutbrudd.
Inn mot 1. verdenskrig (1914-1918) skjedde det et skifte som gikk på at de
internasjonale markedene ble mettet.
Synet på verden endret seg slik at det å bevare freden ikke lenger
ble så interessant for de samme eierene. Parallellen til endringene
mot større ulikhet nå i vår tid er påpekt av flere, også Piketty.

.. Avsnitt Indre-grense-for-vekst

Når det gjelder økonomisk vekst,
så kan det virke som at veksten har en grense som er diktert av
det generelle velstandsnivået.
I Japan har ikke økonomien vokst nevneverdig siden 1990-tallet.
Det har vært en rivende teknologi-utvikling i denne perioden,
men den målbare veksten er likevel overraskende lav.
Resten av den velstående delen av verden følger etter når det gjelder dette mønsteret.
Vekst-teorien til Robert Solow, Solow-modellen, viser at det *er* et gitt
maksimalt nivå for BNP.
Dette baseres på at økning i realkapital har avtagende grensenytte og at en
andel av investeringene må brukes til å motvirke kapitalslitet.
Modellen er forklart i kapittel 19 i boken *Makroøkonomi* av Steinar Holden.
Nyere vekst-modeller tar med flere faktorer,
slik som produktivitets-økning og i innovasjonsevne.
Disse hevder at vi kan ha en kontinuerlig forbedring i produktivitet og innovasjonsevne,
slik at den økonomiske veksten for all evighet kan være positiv.

Det man kan tenke seg er at veksten fortsetter ved at sløsingen øker.
La meg ta klær som et eksempel:
Man kan øke forbruket av klær til et enda høyere nivå enn idag ved å bytte ut
hele garderoben oftere.
Hvis man også sørger for at innsamling av klær opphører,
men at de pent brukte plaggene heller går rett til gjenvinning i form av
f. eks. energiproduksjon (brenning),
vil dette sørge for enda litt økt vekst,
for da blir enda flere sysselsatt med produksjon av nye klær.

Nye innovasjoner vil som nevnt også kunne føre til økt vekst.
For 30 år siden visste vi
jo ikke om alle de nye tingene som skulle komme.
Like lite vet vi hva som kommer til å bli hverdagslige produkter om 30 år.
Den imaginære listen over alt som kan føre til videre vekst synes å være uendelig lang.
Da er det litt påfallende at listen over temaer og diskusjoner om hva som kan
føre til det motsatte, ingen vekst eller negativ vekst,
aldri blir nevnt.
Eller, at hvis disse emnene blir nevnt, så settes de aldri i forbindelse med at
de kan føre til mangel på vekst.
Mat-kasting er ett eksempel. Dersom forbrukerene kaster mindre mat,
flyttes "problemet" bakover i kjeden,
helt til vi står igjen med at vi må produsere mindre mat, som i sin tur gir mindre
aktivitet i hele mat-kjeden.
Tidsklemma er et annet tema.
For å få tid til å ta i bruk de nye innovasjonene, må man slutte med
å konsumere et annet gode,
eller for å si det slik:
På etterspørselssiden vil grensenytten av stadig økt forbruk være avtakende.

Så, holder forutsetningene om kontinuerlig forbedring i produktivitet og innovasjonsevne?
Jeg tror ikke det.
Vil etterspørselen etter varer og tjenester for alltid kunne vokse?
Nei, neppe.
I det lange løp tror jeg Solows teori i all sin enkelhet stemmer ganske bra.
Vi må bare lære oss å leve med det, det *er* et maksimalt nivå for BNP.

.. Avsnitt Noen-vil-redusere-forbruket

Noen ønsker dessuten bevisst å
*redusere* sin etterspørsel etter varer og tjenester.
Dette er særlig de som legger vekt på at økt etterspørsel
antakelig fører til problemer som klimaendringer, og samtidig erkjenner at
livskvaliteten ikke nødvendigvis øker med økt forbruk.

.. Avsnitt Ser-ikke-problemet-med-fravær-av-vekst

Mange er engstelige for at jorden ikke tåler det ekstra uttaket av ressurser som
en ytterligere økning av den økonomiske veksten vil medføre.
Også miljøproblemer som økte mengder CO\ :sub:`2` og andre miljø-belastninger
bekymrer.
Mitt inntrykk er at koblingen til de økonomiske konsekvensene
enten blir underkommunisert eller, enda værre, ikke er forstått av
grupperinger som er forkjempere for et bedre miljø.
Riktignok finnes det økonomer som mener at det ikke finnes noen kobling
mellom økonomisk vekst og forbruk av ressurser.
Mange talsmenn for *et grønt skifte* tenker seg at det er
en vekstprosess i seg selv å skulle skifte ut "fossil" teknologi med fornybar teknologi.
Jeg tror dette er urealistisk ønsketenkning som har oppstått fordi
alt må settes inn i en kontekst av fortsatt vekst for å vinne gehør
hos økonomer, politikere og folk flest.

.. Avsnitt Ikke-troverdige-fortellinger

Status er at vi har økende ulikheter og fraværende vekst i en kultur der "alle vet" at
den eneste løsningen er at vi må vokse oss ut av problemene.
Dessuten er motsetningene mellom storkapitalen og miljøbevegelsen grundig belyst i boken til
Naomi Klein, *This changes everything*.
Dette har konsekvenser:
Politikerene sier gjerne "ja takk, begge deler" og
klarer ikke lenger å gi oss troverdige fortellinger
om hva vi må gjøre for å lage en bedre verden.
Dette gir i sin tur grobunn for ekstreme
politiske retninger der fellesnevneren er at en eller annen gruppe gjøres til syndebukk.

.. Avsnitt Selskaper-må-vokse

Et parallelt problem i denne utviklingen er at alle selskaper vet at det finnes to muligheter
for dem,
voks eller forsvinn.
Problemet her er at alle store selskaper må ekspandere ut over nasjonale
grenser, for hvis hjemmemarkedet er mettet, er det ingen annen mulighet enn å finne
markeder andre steder i verden.
Herav kommer behovet for internasjonale handelsavtaler
som skal sikre selskapenes ekspansjon i fremmede markeder,
for eksempel TTIP og TISA:
Demokratiske prosesser må vike dersom de
truer veksten.

.. Avsnitt Fra-konkurranse-til-krig

Konkurransen mellom selskapene bærer dermed etterhvert mer og mer preg av krig og erobring
når det går mot slutten for mulig ekspansjon.
Krig om markeder og erobring av nye kunde-grupper.
Veien er ikke lang mellom krig som metafor og reell krig.
Nasjonale interesser dreier seg nesten uten unntak om tilgang til ressurser.
Krigsretorikken i store deler av verden går stadig i retning av at vi må ruste opp og
at det er en reell fare for at militære kapasiteter må settes i virksomhet.

.. Avsnitt Andre-forklaringer-på-hvorfor-krig-starter

I Tore Wigs kronikk i Klassekampen 21. januar 2017 "Søvnløs i Trumps tidsalder" redegjør han
for forhandlingsteorien,
og argumenterer med at krigsutbrudd stort sett skjer fordi partene feilberegner hverandres
vilje til å slå tilbake militært på provokasjon.
Han er bekymret over Trump og hans uforutsigbarhet. Jeg er heller ikke beroliget med tanke
på verdensfreden med Trump ved roret i USA, men han er like mye et symptom som selve problemet.
Wig skriver: "Hvis væpnet konflikt er dyrt, og forhandlinger er billige, er forekomsten av
faktiske kriger et mysterium."
Her mener jeg at den vedtatte oppfatningen om at alt skal vokse, alltid, er en fruktbar innfallsport for
å forstå dette mysteriet.
Et stor-samfunn som ser sin eksistens som truet,
bruker de midlene som finnes.
Det kan komme til et punkt der man
ser at det ikke nytter å forhandle fram noe kompromiss.
I et klima der det er alment akseptert alt skal vokse,
og at man ikke får til mer vekst og at man dermed både ser konkret
og føler på seg at samfunnet forvitrer,
så kan krig synes å være et rasjonelt valg.

.. Robert Mood

Robert Mood er bekymret over at avskrekking med atomvåpen ser ut til å bli mer aktuelt igjen
som en del av Natos strategi (Klassekampen 8. april 2017).
Han er redd for at Vesten og Russland ikke lenger klarer å forstå hverandre.
Jeg mener at dynamikken her igjen er at alle vil være sterkest.
Dette er en naturlig utvikling så lenge det er enighet om at det som gjelder her i verden
er å vokse seg å bli stor og sterk.
Dersom noen setter kjepper i hjulene, så må de trues til å sitte rolig i båten.
Slik logikk er jo bare nødt til å gå galt.

.. Avsnitt Må-ha-vekst

Ett av grunnproblemene vi står ovenfor er altså at det ligger i
de nåværende økonomiske institusjonenes design
at vi *må ha* økonomisk vekst.
Konsekvensen av negativ vekst, selvom den er villet,
og dermed ikke kan karakteriseres som en krise,
er at bank- og finansierings-systemene vil bryte sammen.
Systemet slik det er idag fungerer fint der den materielle velstanden var lav,
fordi man der har potensiale for vekst,
men de samfunnene som nærmer seg metningspunktet fungerer dårligere
og dårligere.
Ønsket om å skape flere arbeidsplasser samtidig som vi blir mer produktive
er jo også selvmotsigende;
ingen ser klart for seg hvor nåværende trend kommer til å ende, med vekst som etterhvert ingen
kan gjøre seg nytte av fordi industrien blir ytterligere effektivisert med tap av arbeidplasser som resultat.
Kan det være at det ikke finnes noen løsning på dette paradokset?
At det er helt uunngåelig å ende opp i en stor-konflikt der alle skylder på at det er alle
de andre som er problemet, og ingen helt vet hvorfor vi havnet her?

Det naturlige ville vært å lete etter muligheter for å endre de økonomiske
institusjonene etterhvert som vi opplever at grensen for videre vekst er nådd.
Vi må søke etter måter å skape arbeidsplasser selv uten økonomisk vekst.
Vi må endre hvordan de store internasjonale systemene fungerer;
Vi må få bukt med hvordan dagens banksystem fungerer som et pyramidespill,
som bare fungerer i et klima med vekst.
Boken til Mary Mellor, *Debt or Democracy*,
redegjør for problemer med pengesystemet og hva som kan gjøres for å forbedre
det slik at det kan fungere selv uten vekst.
Vi må planlegge hvordan midler skal spares når akjer og fond slutter å kunne
gi tilbake mer enn vi putter inn (som kun kan skje i et klima med vekst).
Vi må innse at det er en realitet at til tross for iherdige forsøk,
så vil man ikke klare å få veksten til å fortsette evig.

.. Avsnitt Fra-ekspansiv-til-reflektert

I de snart 15 årene jeg har prøvd å sette meg inn i hoved-drivkreftene i samfunnet
har jeg funnet
flere forfattere som peker på momenter som er vesentlige å ta med seg i debatten
om hvor veien går videre.
I tillegg til de som er nevnt over vil jeg trekke fram
Dag Andersen som har skrevet *Det 5. trinn*.
Han deler historien opp i paradigmer, og prøver å forutsi hva neste paradigme vil bestå av.
Han mener at vår intense jakt på materielle goder kommer til å gå over.
Menneskeheten vil bevege seg fra et slags ekspansivt ungdoms-stadium
over i et voksent stadium hvor vi agerer mer modent og reflektert.
Andersen forklarer at mellom hvert paradigme-skifte går det ganske lang tid,
i størrelsesorden 500 år,
og transformasjonen til et nytt paradigme heller ikke er gjort over natten,
men snarere i løpet av en 50 års tid.

.. Avsnitt Hvordan-fra-ekspansiv-til-reflektert

Hvordan kan det være mulig å tenke seg et paradigme-skifte fra en ekspansiv kultur til en mer
reflektert kultur?
I boken til Svein Hammer,
*Fra evig vekst til grønn økonomi* viser han hvordan vekst-samfunnene bredte seg gradvis
utover hele jorden, og at dette skjedde delvis ved å eliminere andre samfunns-strukturer
som ikke var ekspansive av natur.
Dermed er det nærliggende å anta at hvis én nasjon blant mange andre ønsker å bevege seg
mot en *mindre* ekspansiv form,
så vil fort de ekspansive naboene sette en effektiv stopper for denne utviklingen.
Altså vil det ikke være noen nasjon eller noe kontinent som vil ta sjansen på å starte
en slik utvikling på egenhånd.

Den eneste måten jeg kan tenke meg at man kan bryte et slikt mønster på,
er at en stor andel av befolkningen over hele verden innser at riktig vei
å gå er ved å temme iveren etter ekspansjon.
For å få til det må det utarbeides en teori som forklarer hvordan en slik
utvikling er mulig.
Altså at det går an å få til jevnere fordeling av godene og at det går an
å organisere et arbeidsliv selvom
drivkraften i samfunnet ikke er at hver enkelt skal produsere mest mulig
varer og tjenester.
Jeg vil påstå at en økonomisk teori der enkelte episoder betraktes som
'eksterne sjokk' ikke *er* en teori.
Man må forklare fenomenene bank-kriser og krigsutbrudd som en del av teorien.
Man må peke på forløperene til krig og krise og hvordan man unngår disse.
Slike teorier, om de nå allerede finnes, er fullstendig fraværende
fra den offentlige debatten.
Er dette helt urealistisk tankegods?
Det er i alle fall realistisk å si at slik verden ser ut idag,
så er sannsynligheten stor for at det internasjonale
konfliktnivået øker.
Høyre-populisme fosser fram of vinner på at verdens problemer blir forklart ved
å peke på syndebukker.
Venstre-siden har ikke klart å foreta seg annet enn å dilte etter i den
retningen vi alle har blitt indoktrinerte i siden Thatchers og Reagens TINA:
Økonomisk liberalisme.

.. Avsnitt Slutt-appell

**Vi trenger en hypotese om at det er mulig å bli kvitt den
selvdestruktive doktrinen om at alt må vokse, alltid!**

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Vekstens%20paradoks">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/vekstensparadoks/nor/vekstensparadoks.pdf">som pdf</a>
